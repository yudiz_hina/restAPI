require("rootpath")();
var http = require("http"); 
var express = require("express"); 
var bodyParser = require("body-parser");
var morgan = require("morgan");
var util = require('util');
var validator = require('express-validator');

var mysql = require("mysql");
/*=====Custom json/js=====*/
// var config = require("config.json");
var dbConfig = require("./config/database");
var utility = require("./config/utility");

var app = express(); //middleware to have access to req/res object
var router = express.Router();
module.exports = app;

app.use(validator());

app.use('/', router);
/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json({
    limit: '500mb',
    extended: false
}));

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    limit: '500mb',
    extended: true,
    parameterLimit: 50000
}));

/*Include all api */
var users = require("./api/user.js")(utility);
app.use("/api/user",users);

var qb = require('node-querybuilder').QueryBuilder(dbConfig.connection, 'mysql', 'single');

services = require('./modules/queries.js')(qb,utility);

var port = 3001;

var server = http.createServer(app);

server.listen(port, function () {
    console.log("listing..Server Address...", server.address());
});

