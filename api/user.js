const _ = require('lodash');
var express = require('express');
var router = express.Router();
var jwt = require("jsonwebtoken");
var config = require("../config/database");
var authorization = require("./auth");
var nodemailer = require('nodemailer');
var randomString = require('random-string');
module.exports = function(utility){
    router.get('/',function(req,res){
        console.log("Welcome to the user api");
    });

    /*login - start*/
    router.post('/login',function(req,res){
        var status = 404,
        message = "Invalid credentials",
        error = null,
        authToken = null,
        data = {};

        req.checkBody({
            'vEmail': {
                notEmpty: true,
                errorMessage: "Email is required"
            },
            'vPassword': {
                notEmpty: true,
                isLength: {
                    options: [{
                        min: 6,
                        max: 15
                    }],
                    errorMessage: "Password should be more than 6 chars and less than 15 chars"
                },
                errorMessage: "Password is required"
            }
        });
        req.getValidationResult().then(function(result){
            var errors = result.useFirstErrorOnly().mapped();
            if(!result.isEmpty()){
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var object = req.body;
                services.loginUser(object,function(err,row){

                    if(row.length === 1){
                        services.isActive(row[0].iUserId, function(err,rowData){
                            if(rowData[0].eStatus == 'y'){
                                var token = jwt.sign(row[0].iUserId, config.secretKey);
                                var updateData = {
                                    token: token,
                                    iUserId: row[0].iUserId
                                }
                                services.updateAuthToken(updateData, function(err,data){});
                                res.status(200).json({
                                    message: "Login successfully",
                                    data: row[0],
                                    authToken: token,
                                });
                            } else {
                                res.status(401).json({
                                    message: "Your account is not active"
                                });
                            }
                        })
                        
                    } else {
                        res.status(401).json({
                            message: "Invalid credentials"
                        });
                    }
                });
            }
        });
    });
    /*login - end*/

    /*registration - start*/
    router.post("/registration", function(req , res){
        var status = 404,
        message = "Failur",
        errors = null,
        data = {};

        req.checkBody({
            'vName': {
                notEmpty: true,
                errorMessage: "Name is required"
            },
            'vEmail': {
                notEmpty: true,
                isEmail: {
                    errorMessage: "Email id is not valid"
                },
                errorMessage: "Email id is required"
            },
            'vPassword': {
                notEmpty: true,
                isLength: {
                    options: [{
                        min: 6,
                        max: 15
                    }],
                    errorMessage: "Password should be more than 6 chars and less than 15 chars"
                },
                errorMessage: "Password is required"
            }
        });
        req.getValidationResult().then(function (result) {
        var errors = result.useFirstErrorOnly().mapped();
        if (!result.isEmpty()) {
            res.status(400).json({
                message: "Required data are missing",
                error: errors
            });
        } else {
            var object = req.body;           
             services.getUserByEmail(object , function(err, row) {
               if(row.length==0)
                {
                    services.insert(object , function(err, insertData) {
                        if(insertData.insertId>0)
                        {
                            res.status(200).json({
                                message: "You are registered successfully"
                            });
                        }
                    });  
                }else{
                     res.status(200).json({
                        message: "email id already exist"
                     });
                }
            });          
        }
        });               
    });
    /*registration - end*/

    /*List all users - start*/
    router.post("/ListAllUsers", authorization.checkAuthToken, function(req , res){
        var status = 404,
        message = "Failur",
        errors = null,
        data = {};       
        var object = req.body;
        services.getAllUsers(object, function(err, result) {
            if(result.length > 0){
                res.status(200).json({
                    message: "Success",
                    data: result,
                });
            } else {
                res.status(401).json({
                    message: "Data not available"
                });
            }
        });
    });
    /*List all users - end*/

    /*add user - start*/
    router.post("/addUser", function(req , res){
        var status = 404,
        message = "Failur",
        errors = null,
        data = {};

        req.checkBody({
            'vName': {
                notEmpty: true,
                errorMessage: "Name is required"
            },
            'vEmail': {
                notEmpty: true,
                isEmail: {
                    errorMessage: "Email id is not valid"
                },
                errorMessage: "Email id is required"
            },
            'vPassword': {
                notEmpty: true,
                isLength: {
                    options: [{
                        min: 6,
                        max: 15
                    }],
                    errorMessage: "Password should be more than 6 chars and less than 15 chars"
                },
                errorMessage: "Password is required"
            }
        });
        req.getValidationResult().then(function (result) {
            var errors = result.useFirstErrorOnly().mapped();
            if (!result.isEmpty()) {
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var object = req.body;           
                services.getUserByEmail(object , function(err, row) {
                if(row.length==0)
                    {
                        services.insert(object , function(err, insertData) {
                            if(insertData.insertId>0)
                            {
                                res.status(200).json({
                                    message: "User added successfully"
                                });
                            }
                        });  
                    }else{
                        res.status(200).json({
                            message: "email id already exist"
                        });
                    }
                });          
            }
        });               
    });
    /*add user - end*/

    /*delete user - start*/
    router.post("/deleteUser", function(req , res){
        var status = 404,
        message = "Failur",
        errors = null,
        data = {};       
        req.checkBody({
            'iUserId': {
                notEmpty: true,
                errorMessage: "User id is required"
            }
        });
        req.getValidationResult().then(function (result) {
        var errors = result.useFirstErrorOnly().mapped();
        if (!result.isEmpty()) {
            res.status(400).json({
                message: "Required data are missing",
                error: errors
            });
        } else {
            var object = req.body;           
             services.deleteUser(object , function(err, result) {
                if(result.affectedRows > 0)
                {
                    res.status(200).json({
                        message: "Success"
                    });
                         
                } else {
                    res.status(401).json({
                        message: "Failur"
                    });
                }
            });          
        }
        });
        
    });
    /*delete user - end*/

    /*update user - start*/
    router.post('/updateUser', function(req,res){
        var status = 404,
        message = 'Failur',
        error = null,
        data = {};

        req.checkBody({
            'vName': {
                notEmpty: true,
                errorMessage: "Name is required"
            },
            'vEmail': {
                notEmpty: true,
                isEmail: {
                    errorMessage: "Email id is not valid"
                },
                errorMessage: "Email id is required"
            },
            'iUserId': {
                notEmpty: true,
                errorMessage: "User id is required"
            }
        });
        req.getValidationResult().then(function (result) {
            var errors = result.useFirstErrorOnly().mapped();
            if (!result.isEmpty()) {
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var object = req.body;    
                services.updateUserData(object , function(err, updateData) {
                    console.log(updateData);
                    if(updateData.affectedRows>0)
                    {
                        res.status(200).json({
                            message: "User edited successfully"
                        });
                    }
                });               
            }
        }); 

    });
    /*update user - end*/

    /*change status - start*/
    router.post('/changeStatus', authorization.checkAuthToken, function(req,res){
        var status = 404,
        message = 'Failur',
        error = null,
        data = {};

        req.checkBody({
            'eStatus': {
                notEmpty: true,
                errorMessage: "Status is required"
            },
            'iUserId': {
                notEmpty: true,
                errorMessage: "User id is required"
            }
        });
        req.getValidationResult().then(function (result) {
            var errors = result.useFirstErrorOnly().mapped();
            if (!result.isEmpty()) {
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var object = req.body;    
                services.changeStatus(object , function(err, updateData) {
                    // console.log(updateData);
                    if(updateData.affectedRows>0)
                    {
                        res.status(200).json({
                            message: "Status changed successfully"
                        });
                    }
                });     
            }
        }); 

    });
    /*change status - end*/

    /*change password - start*/
    router.post('/changePassword', authorization.checkAuthToken, function(req,res){
        var status = 404,
        message = 'Failur',
        error = null,
        data = {};
        var object = req.body;
        services.checkOldPassword(object, function(err,resultPwd){
            if(resultPwd.length === 1){
                req.checkBody('iUserId', 'Userid is required').notEmpty();
                req.checkBody('vOldPassword', 'Old Password is required').notEmpty();
                req.checkBody('vNewPassword', 'New Password is required').notEmpty();
                req.checkBody('vNewPassword', 'Password should be more than 6 chars and less than 15 chars').isLength(options = {min:6, max:15});
                req.checkBody('vConfirmPassword', 'Confirm Password is required').notEmpty();
                req.checkBody('vConfirmPassword', 'Passwords do not match').equals(object.vNewPassword);
                
                req.getValidationResult().then(function (result) {
                    var errors = result.useFirstErrorOnly().mapped();
                    if (!result.isEmpty()) {
                        res.status(400).json({
                            message: "Required data are missing",
                            error: errors
                        });
                    } else {
                        services.changePassword(object , function(err, updateData) {
                            if(updateData.affectedRows>0)
                            {
                                res.status(200).json({
                                    message: "Password changed successfully"
                                });
                            }
                        });               
                    }
                });
            } else {
                res.status(401).json({
                    message: "Old password is wrong"
                });
            }
        });
    });
    /*change password - end*/

    /*forgot password - start*/
    router.post('/forgotPassword', function(req,res){
        var status = 404,
        message = 'Failur',
        error = null,
        data = {};
        var object = req.body;
        
        req.checkBody({
            'vEmail': {
                notEmpty: true,
                errorMessage: "Email id is required"
            }
        });
        req.getValidationResult().then(function (result) {
            var errors = result.useFirstErrorOnly().mapped();
            if (!result.isEmpty()) {
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var object = req.body;    
                services.getUserByEmail(object , function(err, result) {
                    if(result.length > 0)
                    {
                        if(result[0].eStatus == 'n'){
                            res.status(412).json({
                                message: "Your account is not active"
                            });
                        } else if(result[0].eStatus == 'd'){
                            res.status(412).json({
                                message: "Your account deleted"
                            });
                        } else {
                            var transporter = nodemailer.createTransport(config.nodeMailerTransporter);
                            var randomPassword = randomString({length: 6,letters:true});
                            var mailOptions = {
                                from: 'hina@ping2world.com', // sender address
                                to: object.vEmail, // list of receivers
                                subject: 'One time password', // Subject line
                                html: 'Here is your one time password <b>' + randomPassword + '</b>'// html body
                            };
                            transporter.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                } else {
                                    var data ={
                                        iUserId: result[0].iUserId,
                                        vNewPassword: randomPassword
                                    }
                                    services.changePassword(data,function(err,row){
                                        if(row.affectedRows>0){
                                            res.status(200).json({
                                                message: "One time password sent to your email id"
                                            });
                                        }
                                    });
                                    console.log('Message %s sent: %s', info.messageId, info.response);
                                }
                            });
                        }
                    } else {
                        res.status(412).json({
                            message: "User not found"
                        });
                    }
                });               
            }
        });
    });
    /*forgot password - end*/

    /*login with social media - fb - start*/
    router.post('/fbLogin', function(req,res){
        var status = 404,
        message = 'Failur',
        error = null,
        data = {};

        req.checkBody({
            'vFacebookId': {
                notEmpty: true,
                errorMessage: "Facebook is required"
            }
        });
        req.getValidationResult().then(function (result) {
            var errors = result.useFirstErrorOnly().mapped();
            if (!result.isEmpty()) {
                res.status(400).json({
                    message: "Required data are missing",
                    error: errors
                });
            } else {
                var body = _.pick(req.body, ['vName','vEmail','vFacebookId']);
                var object = body;
                
                    services.fbLogin(object , function(err, updateData) {
                        if(updateData.length === 1)
                        {
                            res.status(200).json({
                                message: "Loggedin successfully",
                                data: updateData[0]
                            });
                        }
                    });
                
                
                     
            }
        });

    });
    /*login with social media - fb - end*/

    return router;
}