var dateFormat = require('dateformat');
var nowDat = function(){
    return dateFormat(new Date(), "UTC:yyyy-mm-dd HH:MM:SS",true);
};
module.exports = nowDat;