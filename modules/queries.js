var md5 = require('md5');
module.exports = function(queryBuilder,utility){
    var query = {
        loginUser: function(param, callback){
            var vPassword = md5(param.vPassword);
            queryBuilder.select('iUserId,vName,vEmail,dCreatedDate,eStatus').where({vEmail : param.vEmail,vPassword : vPassword}).get('tbl_users',callback);
        },
        insert: function(param, callback){
            var insertData = {
                vName: param.vName,
                vEmail: param.vEmail,
                vPassword: md5(param.vPassword),
                dCreatedDate: utility(),
                dModifiedDate: utility()
            };
            queryBuilder.insert('tbl_users',insertData,callback);
        },
        getUserByEmail: function(param,callback){
            queryBuilder.select('*').where({vEmail: param.vEmail}).get('tbl_users',callback);
        },
        getUserById: function(param,callback){
            queryBuilder.select('*').where({iUserId: param.iUserId}).get('tbl_users',callback);
        },
        getAllUsers: function(param,callback){
            queryBuilder.select('*').where({'eStatus !=': 'd'}).limit(param.iLimit,param.iOffset).order_by('iUserId','desc').get('tbl_users',callback);
        },
        deleteUser: function(param,callback){
            var deleteData = {
                eStatus : 'd'
            }
            queryBuilder.update('tbl_users', deleteData, {iUserId: param.iUserId}, callback);
        },
        updateUserData: function(param, callback){
            var updateData = {
                vName : param.vName,
                vEmail : param.vEmail,
                dModifiedDate: utility()
            }
            queryBuilder.update('tbl_users', updateData, {iUserId :param.iUserId}, callback);
        },
        changeStatus: function(param, callback){
            var finalData = param.eStatus == '1' ? 'y' : 'n';
            var data = {
                eStatus : finalData,
                dModifiedDate: utility()
            }
            queryBuilder.update('tbl_users', data, {iUserId:param.iUserId}, callback);
        },
        isActive : function(param, callback){
            queryBuilder.select('eStatus').where({iUserId: param}).get('tbl_users',callback);
        },
        changePassword : function(param, callback){
            var updateData = {
                vPassword : md5(param.vNewPassword)
            }
            queryBuilder.update('tbl_users', updateData, {iUserId:param.iUserId}, callback);
        },
        checkOldPassword : function(param, callback){
            var password = md5(param.vOldPassword)
            queryBuilder.select('*').where({iUserId: param.iUserId, vPassword: password}).get('tbl_users',callback);
        },
        fbLogin: function(param, callback){
            queryBuilder.select('*').where('vFacebookId', param.vFacebookId).or_where('vEmail', param.vEmail).get('tbl_users', function(err,result){
                if(result != ''){
                    var updateData = {
                        vName : param.vName,
                        vEmail : param.vEmail,
                        vFacebookId: param.vFacebookId,
                        eStatus : 'y',
                        dModifiedDate: utility()
                    }
                    queryBuilder.update('tbl_users', updateData, {iUserId: result[0].iUserId}, function(error,res){
                        queryBuilder.select('iUserId,vName,vEmail,vFacebookId,dCreatedDate,eStatus').where({iUserId: result[0].iUserId}).get('tbl_users',callback);
                    });
                } else {
                    var insertData = {
                        vName: param.vName,
                        vEmail: param.vEmail,
                        vFacebookId: param.vFacebookId,
                        dCreatedDate: utility(),
                        dModifiedDate: utility()
                    }
                    queryBuilder.insert('tbl_users',insertData, function(error, res){
                        queryBuilder.select('iUserId,vName,vEmail,vFacebookId,dCreatedDate,eStatus').where({iUserId : res.insertId}).get('tbl_users',callback);
                    });
                }
            });
        },
        updateAuthToken: function(param,callback){
            var data = {
                vAuthToken : param.token,
                dModifiedDate: utility()
            }
            queryBuilder.update('tbl_users', data, {iUserId:param.iUserId}, callback);
        },
        checkLogin: function(param,callback){
            queryBuilder.select('*').where({iUserId : param.iUserId, vAuthToken: param.authToken}).get('tbl_users',callback);
        }
    };
    return query;
};